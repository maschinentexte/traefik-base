#!/bin/bash

echo "Setting up the traefik docker network (Don't worry if it fails)"
docker network create traefik

echo "Creating a new configuration in the .env file."
echo "Press Ctl-C if you don't need one"

echo "# New configuration from $(date)" >> .env

read -p 'Which email do you use for Letsencrypt: ' ACME_EMAIL
echo "ACME_EMAIL=${ACME_EMAIL}" >> .env

read -p 'What is the domain name of this server: ' SERVER_DOMAIN
echo "SERVER_DOMAIN=${SERVER_DOMAIN}" >> .env

read -p 'Please define a username to login to the Traefik dashboard: ' user
read -sp 'Please define a password to login to the Traefik dashboard: ' pass
echo '\n'

auth=$(htpasswd -nb $user $pass)

echo "TRAEFIK_AUTH=${auth}" >> .env

echo '' >> .env

echo 'Done!'