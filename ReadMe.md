# A base Traefik server in a Docker Compose

This docker compose starts a traefik server listening on a docker network for other containers to be proxied to.
These other containers need to connect to the same network. See the example directory for a docker-compose.yml

To install the latest docker, try running the `./install-docker.sh`. It will install docker and docker-compose on Ubuntu.

Before you run this, run the setup script first with `./setup.sh`.

After the `.env` file has been successfully created, you can start the Traefik server with

```
docker-compose up -d
```

To see any live logs run:

```
docker-compose logs -f
```